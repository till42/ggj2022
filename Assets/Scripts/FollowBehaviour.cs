using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBehaviour : MonoBehaviour
{
    [SerializeField] private Transform _objectToFollow;

    private Vector3 _offset;

    private void Start()
    {
        _offset = transform.position - _objectToFollow.position;
    }

    private void Update()
    {
        transform.position = _objectToFollow.position + _offset;
    }
}