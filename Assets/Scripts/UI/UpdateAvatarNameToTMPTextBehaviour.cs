using System;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using Photon.Pun;
using TMPro;
using UnityEngine;

namespace Seesaw.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class UpdateAvatarNameToTMPTextBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableString _playerName;

        private void Start()
        {
            var tmpText = GetComponent<TMP_Text>();
            var photonView = GetComponentInParent<PhotonView>();
            if (!PhotonNetwork.IsConnected || photonView == null)
            {
                tmpText.text = $"{_playerName} (you)";
                return;
            }

            var playerName = photonView.Owner.NickName;
            if (photonView.IsMine)
                playerName += " (you)";
            tmpText.text = playerName;
        }
    }
}