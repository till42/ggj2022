using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FollowMousePositionBehaviour : MonoBehaviour
{
    [SerializeField] private LayerMask _layerMask;


    private Vector3 _offset;
    private Camera _camera;

    private void Awake() => _camera = Camera.main;

    private void Start() => _offset = transform.position;

    void Update()
    {
        RaycastHit hit;
        var ray = _camera.ScreenPointToRay(Input.mousePosition);

        if (!Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask)) return;

        transform.position = hit.point + _offset;
    }
}