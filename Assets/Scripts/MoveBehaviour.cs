using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public class MoveBehaviour : MonoBehaviour
{
    [SerializeField] private float _amplitude = 1f;


    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Move(CallbackContext context)
    {
        var direction = context.ReadValue<Vector2>();
        var forceDirection = new Vector3(direction.x, 0f, direction.y) * _amplitude;
        _rigidbody.AddForce(forceDirection);
    }
}