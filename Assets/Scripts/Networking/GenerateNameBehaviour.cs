using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using Seesaw.Extensions;
using UnityEngine;

namespace Seesaw.Networking
{
    public class GenerateNameBehaviour : MonoBehaviour, IStringGenerator
    {
        [SerializeField] private List<string> _adjectives;
        [SerializeField] private List<string> _nouns;

        public string Generate()
        {
            var result = GenerateName(_adjectives, _nouns);
            return result;
        }

        private string GenerateName(List<string> prefixes, List<string> middleParts)
        {
            var prefix = prefixes.PickRandomItem().FirstCharToUpper();

            var middlePart = middleParts.PickRandomItem().FirstCharToUpper();

            return $"{prefix} {middlePart}";
        }
    }
}