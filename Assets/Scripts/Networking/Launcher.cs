using System;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Seesaw.Networking
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        private readonly string _gameVersion = "1";

        [Tooltip(
            "The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        [SerializeField]
        private byte _maxPlayersPerRoom = 2;

        [Tooltip("The Ui Panel to let the user enter name, connect and play")] [SerializeField]
        private GameObject _controlPanel;

        [SerializeField] private ScriptableString _statusText;


        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            _statusText.Value = "";
            _controlPanel.SetActive(true);
        }

        public void Connect()
        {
            _statusText.Value = "Connecting...";
            _controlPanel.SetActive(false);
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = _gameVersion;
            }
        }

        public override void OnConnectedToMaster()
        {
            _statusText.Value = "Connected to server";
            PhotonNetwork.JoinRandomRoom();
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            var roomname = PhotonNetwork.NickName;

            _statusText.Value = $"Created Room with name {roomname}. Waiting for other player";

            PhotonNetwork.CreateRoom(roomname, new RoomOptions {MaxPlayers = _maxPlayersPerRoom});
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            _statusText.Value = $"Player Entered Room: {newPlayer.NickName}";
            var room = PhotonNetwork.CurrentRoom;
            if (room.PlayerCount >= room.MaxPlayers)
            {
                _statusText.Value = "Starting Game";
                PhotonNetwork.LoadLevel(1);
            }
        }

        public override void OnJoinedRoom()
        {
            if (PhotonNetwork.IsMasterClient) return;

            var room = PhotonNetwork.CurrentRoom;
            _statusText.Value = $"Joined Room with {room.Name}";
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reason {0}",
                cause);
            _statusText.Value = "Disconnected";
            _controlPanel.SetActive(true);
        }
    }
}