using System;
using Cinemachine;
using UnityEngine;

namespace Seesaw.Networking
{
    public class AttachSelfAsVirtualFollowTarget : MonoBehaviour
    {
        private void Start()
        {
            var virtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
            if (virtualCamera == null) return;
            virtualCamera.Follow = transform;
        }
    }
}