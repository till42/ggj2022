using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace Seesaw.Networking
{
    public class DisconnectBehaviour : MonoBehaviour
    {
        public UnityEvent OnDisconnected;

        void OnEnable()
        {
            PhotonNetwork.Disconnect();
        }
    }
}