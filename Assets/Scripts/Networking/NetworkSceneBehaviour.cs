using FantasyArts.Tools.Runtime.Architecture.Scene;
using Photon.Pun;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Seesaw.Networking
{
    public class NetworkSceneBehaviour : MonoBehaviour
    {
        [SerializeField] private SceneReference _scene;

        [SerializeField] private bool _doesLoadScenesNotInBuild;

        [ContextMenu("Load")]
        public void Load()
        {
#if UNITY_EDITOR
            if (_doesLoadScenesNotInBuild)
            {
                EditorSceneManager.LoadSceneInPlayMode(_scene.ScenePath, new LoadSceneParameters(LoadSceneMode.Single));
                return;
            }
#endif

            SceneManager.LoadScene(_scene);
        }

        public void Reload()
        {
            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        public void NetworkLoad()
        {
            if (!PhotonNetwork.IsConnected) Load();
            PhotonNetwork.LoadLevel(_scene);
        }
    }
}