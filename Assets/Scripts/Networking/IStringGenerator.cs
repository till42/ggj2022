﻿namespace Seesaw.Networking
{
    public interface IStringGenerator
    {
        string Generate();
    }
}