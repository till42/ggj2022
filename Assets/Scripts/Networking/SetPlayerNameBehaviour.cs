using System;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using Photon.Pun;
using UnityEngine;

namespace Seesaw.Networking
{
    public class SetPlayerNameBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableString _playerName;

        private const string PlayerNamePrefKey = "PlayerName";

        private void Awake() => ReadName();

        private void ReadName()
        {
            var playerName = GetName();
            SetName(playerName);
        }

        [ContextMenu("Refresh")]
        public void Refresh()
        {
            PlayerPrefs.DeleteKey(PlayerNamePrefKey);
            ReadName();
        }

        private string GetName()
        {
            if (PlayerPrefs.HasKey(PlayerNamePrefKey))
                return PlayerPrefs.GetString(PlayerNamePrefKey);

            var generator = GetComponent<IStringGenerator>();
            return generator.Generate();
        }

        private void SetName(string playerName)
        {
            _playerName.Value = playerName;
            PhotonNetwork.NickName = playerName;
            PlayerPrefs.SetString(PlayerNamePrefKey, playerName);
        }
    }
}