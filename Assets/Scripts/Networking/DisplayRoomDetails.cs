using System;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Seesaw.Networking
{
    public class DisplayRoomDetails : MonoBehaviour
    {
        [SerializeField] private TMP_Text _tmp;

        [SerializeField] private Transform _position1;
        [SerializeField] private Transform _position2;

        private void Start()
        {
            if (!PhotonNetwork.IsConnected)
            {
                Write("Not connected to Photon");
                return;
            }

            var room = PhotonNetwork.CurrentRoom;
            if (room == null)
            {
                Write("No room found");
                return;
            }

            string s = "";
            s += $"Room name: {room.Name}\n";
            foreach (var keyValue in room.Players)
            {
                s += $"[{keyValue.Key}] {keyValue.Value.NickName}\n";
            }

            if (PhotonNetwork.IsMasterClient)
            {
                s += "You are the master";
                InstantiateAvatarAt(_position1);
            }
            else
            {
                s += "You are not the master";
                InstantiateAvatarAt(_position2);
            }


            Write(s);
        }

        private void InstantiateAvatarAt(Transform parent)
        {
            var position = parent.position;
            PhotonNetwork.Instantiate("Network Avatar", position, Quaternion.identity, 0);
        }

        private void Write(string text)
        {
            _tmp.text = text;
        }
    }
}