using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Seesaw.Networking
{
    public class GameSceneInitializer : MonoBehaviourPunCallbacks
    {
        [SerializeField] private ScriptableEvent _gameOverEvent;


        public static GameSceneInitializer EvilSingleton;

        public Transform FallingAvatarSpawnPosition;
        public Transform JumpingAvatarSpawnPosition;

        public GameObject SingleplayerFallingAvatar;
        public GameObject SingleplayerJumpingAvatar;

        public PowerGauge Gauge;

        public TextMeshProUGUI AppleCounter;
        public Image AppleUIImage;

        private int _appleCollected;

        public UnityEvent<int> OnApplePicked;


        private void Awake()
        {
            EvilSingleton = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            if (!PhotonNetwork.IsConnected)
                return;

            Destroy(SingleplayerFallingAvatar);
            Destroy(SingleplayerJumpingAvatar);

            if (PhotonNetwork.IsMasterClient)
            {
                // Spawn faller
                PhotonNetwork.Instantiate("Avatar1", FallingAvatarSpawnPosition.position,
                    FallingAvatarSpawnPosition.rotation);
            }
            else
            {
                // Spawn jumper
                PhotonNetwork.Instantiate("Avatar2", JumpingAvatarSpawnPosition.position,
                    JumpingAvatarSpawnPosition.rotation);
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            _gameOverEvent.Raise();
        }

        public void AppleCollected(int amount)
        {
            _appleCollected += amount;

            AppleCounter.text = _appleCollected + "x";
            OnApplePicked?.Invoke(amount);
        }
    }
}