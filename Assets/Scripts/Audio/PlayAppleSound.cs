using System;
using UnityEngine;

namespace Seesaw.Audio
{
    public class PlayAppleSound : MonoBehaviour
    {
        [SerializeField] private AudioClip _goodApple;
        [SerializeField] private AudioClip _badApple;

        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void Play(int amount)
        {
            var clip = amount >= 0 ? _goodApple : _badApple;
            _audioSource.PlayOneShot(clip);
        }
    }
}