using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seesaw.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayAudioListBehaviour : MonoBehaviour
    {
        [SerializeField] private List<AudioClip> _clips;

        private AudioSource _audioSource;

        private IEnumerator Start()
        {
            _audioSource = GetComponent<AudioSource>();
            var queue = new Queue<AudioClip>(_clips);

            while (queue.Count > 0)
            {
                var clip = queue.Dequeue();
                var length = clip.length;
                _audioSource.clip = clip;
                _audioSource.Play();
                if (queue.Count <= 0)
                    _audioSource.loop = true;
                else
                    _audioSource.loop = false;
                yield return new WaitForSeconds(length);
            }
        }
    }
}