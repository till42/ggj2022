using UnityEngine;
using UnityEngine.InputSystem;

namespace Seesaw.Input
{
    public class EnableInputBehaviour : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _asset;

        private void OnEnable()
            => _asset.Enable();
    }
}