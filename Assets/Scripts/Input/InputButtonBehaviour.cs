using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Seesaw.Input
{
    public class InputButtonBehaviour : MonoBehaviour
    {
        [SerializeField] private InputActionReference _actionReference;

        public UnityEvent OnButtonDown;
        public UnityEvent OnButtonUp;

        private void OnEnable()
            => RegisterActionEvents(_actionReference.action);

        private void RegisterActionEvents(InputAction action)
        {
            action.started += HandleStarted;
            action.canceled += HandleCanceled;
        }

        private void HandleStarted(CallbackContext context)
            => OnButtonDown?.Invoke();

        private void HandleCanceled(CallbackContext context)
            => OnButtonUp?.Invoke();

        private void OnDisable()
            => UnregisterActionEvents(_actionReference.action);

        private void UnregisterActionEvents(InputAction action)
        {
            action.started -= HandleStarted;
            action.canceled -= HandleCanceled;
        }
    }
}