﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Seesaw.Logic
{
    public class CollidersInRangeBehaviour : MonoBehaviour, IColliderCollection
    {
        [SerializeField] private string _tag;

        private readonly HashSet<Collider2D> _collidersInRange = new();
        public IReadOnlyCollection<Collider2D> Colliders => _collidersInRange;

        public event Action OnChanged;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.CompareTag(_tag)) return;
            _collidersInRange.Add(col);
            OnChanged?.Invoke();
        }

        private void OnTriggerExit2D(Collider2D col)
        {
            if (!col.CompareTag(_tag)) return;
            _collidersInRange.Remove(col);
            OnChanged?.Invoke();
        }
    }
}