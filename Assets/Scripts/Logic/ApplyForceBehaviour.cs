using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace Seesaw.Logic
{
    public class ApplyForceBehaviour : MonoBehaviour
    {
        [SerializeField] private float _amplifier = 5f;
        public Animator Animator;

        private IColliderCollection _inRange;

        public UnityEvent OnApplied;

        private void Awake() => _inRange = GetComponent<IColliderCollection>();

        public void Apply(Vector2 force)
        {
            foreach (var col in _inRange.Colliders)
            {
                if (PhotonNetwork.IsConnected)
                {
                    var photonView = col.GetComponentInParent<PhotonView>();
                    if (photonView == null) continue;
                    photonView.RPC("ReceiveForce", RpcTarget.All, force);
                    Debug.Log($"Applied force {force} to {col}", col);
                    OnApplied?.Invoke();
                    Animator.SetTrigger("Jumped");
                }
                else
                {
                    var receiver = col.GetComponentInParent<ReceiveForceBehaviour>();
                    if (receiver == null)
                        continue;
                    receiver.ReceiveForce(force);
                    OnApplied?.Invoke();
                    Animator.SetTrigger("Jumped");
                }
            }
        }
    }
}