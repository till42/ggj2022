using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Seesaw
{
    public class PowerGauge : MonoBehaviour
    {
        public AnimationCurve SpeedCurve;
        public AnimationCurve ValuesCurve;
        public float Speed = 1f;

        public Transform Needle;
        public Vector2 RotationLimits;
        public TextMeshProUGUI DisplayText;

        private float _currentProgress;
        public float CurrentProgress => _currentProgress;

        private enum GaugeDirection
        {
            Left,
            Right
        }

        private GaugeDirection _currentDirection = GaugeDirection.Right;
        private bool _stopped;

        // Update is called once per frame
        void Update()
        {
            if (_stopped) return;

            var rotation = Mathf.Lerp(RotationLimits.x, RotationLimits.y, _currentProgress);
            SetRotation(rotation);

            var currentSpeed = SpeedCurve.Evaluate(_currentProgress) * Speed;

            if (_currentDirection == GaugeDirection.Right)
            {
                _currentProgress = Mathf.Min(_currentProgress + Time.deltaTime * currentSpeed, 1f);
                if (_currentProgress >= 1f) _currentDirection = GaugeDirection.Left;
            }
            else
            {
                _currentProgress = Mathf.Max(_currentProgress - Time.deltaTime * currentSpeed, 0f);
                if (_currentProgress <= 0f) _currentDirection = GaugeDirection.Right;
            }

            var power = GetCurrentPower();
            DisplayText.text = (power * 100f).ToString("0");
            DisplayText.transform.localScale = Vector3.Lerp(Vector3.one * 0.01f, Vector3.one * 0.02f, power);
        }

        private void SetRotation(float rotation)
        {
            var angles = Needle.localEulerAngles;
            angles.z = rotation;
            Needle.localEulerAngles = angles;
        }

        public float GetCurrentPower()
        {
            return ValuesCurve.Evaluate(_currentProgress);
        }

        public void StopMoving()
        {
            _stopped = true;
        }

        public void SetPower(float power, float progress)
        {
            StopMoving();
            var rotation = Mathf.Lerp(RotationLimits.x, RotationLimits.y, progress);
            SetRotation(rotation);
            _currentProgress = progress;
            DisplayText.text = (power * 100f).ToString("0");
            DisplayText.transform.localScale = Vector3.Lerp(Vector3.one * 0.01f, Vector3.one * 0.02f, power);
        }

        public void StartMoving()
        {
            _stopped = false;
        }
    }
}