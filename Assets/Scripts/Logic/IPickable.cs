using UnityEngine;

namespace Seesaw.Logic
{
    public interface IPickable
    {
        void Picked();
        void Highlight();
        void UnHighlight();
    }
}