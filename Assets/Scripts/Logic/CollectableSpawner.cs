using Photon.Pun;
using Seesaw.Logic;
using System.Collections;
using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using UnityEngine;

namespace Seesaw
{
    public class CollectableSpawner : MonoBehaviour
    {
        public float Width = 2f;
        public float AverageHeightBetweenApples = 2f;
        public int ToSpawn = 15;
        public AnimationCurve Distribution;

        [SerializeField] private List<GameObject> _prefabs;


        private List<GameObject> _spawned = new();

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(SpawnAsync());
        }


        private IEnumerator SpawnAsync()
        {
            _spawned.Clear();
            foreach (var pos in GetSpawnPositions())
            {
                SpawnAtPosition(pos);

                yield return null;
            }
        }

        private void SpawnAtPosition(Vector3 position)
        {
            var prefab = _prefabs.PickRandomItem();
            var prefabName = prefab.name;
            if (!PhotonNetwork.IsConnected)
            {
                _spawned.Add(Instantiate(Resources.Load<GameObject>(prefabName), position, Quaternion.identity));
                return;
            }

            if (!PhotonNetwork.IsMasterClient)
                _spawned.Add(PhotonNetwork.Instantiate(prefabName, position, Quaternion.identity));
        }

        public void OnDrawGizmosSelected()
        {
            GetSpawnPositions().ForEach(x =>
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(x, 0.2f);
            });
        }

        private List<Vector3> GetSpawnPositions()
        {
            var toReturn = new List<Vector3>();
            var curPos = transform.position;
            var height = ToSpawn * AverageHeightBetweenApples;

            for (int i = 0; i < ToSpawn; i++)
            {
                var actualPos = curPos;
                curPos.y += Distribution.Evaluate(Random.value) * AverageHeightBetweenApples;
                actualPos.x += Random.Range(-Width / 2f, Width / 2f);
                toReturn.Add(actualPos);
            }

            return toReturn;
        }
    }
}