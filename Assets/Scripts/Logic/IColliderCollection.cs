﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Seesaw.Logic
{
    public interface IColliderCollection
    {
        IReadOnlyCollection<Collider2D> Colliders { get; }
        event Action OnChanged;
    }
}