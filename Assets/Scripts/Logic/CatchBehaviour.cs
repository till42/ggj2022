using System.Linq;
using UnityEngine;

namespace Seesaw.Logic
{
    public class CatchBehaviour : MonoBehaviour
    {
        private IColliderCollection _inRange;

        private IPickable _cachedClosest;

        private void Awake()
        {
            _inRange = GetComponent<IColliderCollection>();
            _inRange.OnChanged += CollidersChanged;
        }

        public void Catch()
        {
            foreach (var col in _inRange.Colliders)
            {
                var pickable = col.GetComponent<IPickable>();
                if (pickable == null) continue;

                pickable.Picked();
                Debug.Log($"Picked {pickable}", col);
                break;
            }
        }

        public void CollidersChanged()
        {
            var closest = _inRange.Colliders
                .OrderBy(x => Vector2.Distance(x.transform.position, transform.position))
                .FirstOrDefault();

            if (closest == null)
            {
                _cachedClosest?.UnHighlight();
                _cachedClosest = null;
                return;
            }

            var closestPickable = closest.GetComponent<IPickable>();

            if(closestPickable != null && closestPickable != _cachedClosest)
            {
                closestPickable.Highlight();
                _cachedClosest?.UnHighlight();

                _cachedClosest = closestPickable;
            }
        }
    }
}