using System;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace Seesaw.Logic
{
    public class RaiseEventBehaviour : MonoBehaviour
    {
        [SerializeField] private string _tag = "Player";

        [SerializeField] private ScriptableEvent _event;

        private bool _isActive;

        public void Activate()
        {
            _isActive = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!_isActive) return;
            if (!other.CompareTag(_tag)) return;
            _event.Raise();
        }
    }
}