using System;
using Photon.Pun;
using UnityEngine;

namespace Seesaw.Logic
{
    public class PushUpBehaviour : MonoBehaviour
    {
        [SerializeField] private float _amplifier = 2f;

        private Rigidbody2D _rigidbody2D;
        private PhotonView _photonView;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _photonView = GetComponent<PhotonView>();
        }

        [ContextMenu("Push")]
        public void Push()
        {
            if (_photonView != null && !_photonView.IsMine) return;
            _rigidbody2D.AddForce(Vector2.up * _amplifier, ForceMode2D.Impulse);
        }
    }
}