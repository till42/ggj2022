using System;
using System.Collections;
using Photon.Pun;
using Seesaw.Networking;
using UnityEngine;

namespace Seesaw.Logic
{
    public class PickedBehaviour : MonoBehaviour, IPickable
    {
        public Animator Animator;

        [SerializeField] private int _amount = 1;

        private PhotonView _photonView;

        private void Awake()
        {
            _photonView = GetComponent<PhotonView>();
        }

        public void Highlight()
        {
            Animator.SetBool("Highlighted", true);
        }

        public void UnHighlight()
        {
            Animator.SetBool("Highlighted", false);
        }

        public void Picked()
        {
            if (!PhotonNetwork.IsConnected)
            {
                NetworkDestroy();
            }
            else
            {
                if (!_photonView.IsMine) return;
                _photonView.RPC("NetworkDestroy", RpcTarget.All);
            }
        }

        [PunRPC]
        public void NetworkDestroy()
        {
            GameSceneInitializer.EvilSingleton.AppleCollected(_amount);
            Destroy(gameObject);
        }
    }
}