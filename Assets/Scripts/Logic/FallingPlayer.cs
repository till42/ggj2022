using Photon.Pun;
using Seesaw.Networking;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Seesaw.Logic
{
    public class FallingPlayer : MonoBehaviour
    {
        public float GuagePower = 2f;

        public UnityEvent OnFalling;

        private Animator _animator;
        private Rigidbody2D _rigidbody;
        private float _initialGravityScale;
        private PhotonView _photonView;

        private bool _hasFallen;

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
            _rigidbody = GetComponent<Rigidbody2D>();
            Assert.IsNotNull(_rigidbody, "Rigidbody2D is missing on FallingPlayer");
            _initialGravityScale = _rigidbody.gravityScale;

            _photonView = GetComponent<PhotonView>();
        }

        public void StartFalling()
        {
            if (_hasFallen) return;
            if (!PhotonNetwork.IsConnected)
            {
                FallOffline();
                return;
            }

            if (_photonView != null && !_photonView.IsMine)
                return;

            var gauge = GameSceneInitializer.EvilSingleton.Gauge;
            var gravityScale = gauge.GetCurrentPower();
            var progress = gauge.CurrentProgress;

            _photonView.RPC("SetGravity", RpcTarget.All, gravityScale, progress);
        }

        private void FallOffline()
        {
            _animator.SetTrigger("Fall");
            var gauge = GameSceneInitializer.EvilSingleton.Gauge;
            gauge.StopMoving();
            var power = gauge.GetCurrentPower() * GuagePower;
            _rigidbody.gravityScale = _initialGravityScale + power;
            _rigidbody.simulated = true;
            OnFalling?.Invoke();
            _hasFallen = true;
        }

        [PunRPC]
        public void SetGravity(float gravityScale, float progress)
        {
            _animator.SetTrigger("Fall");
            Debug.Log($"SetGravity({gravityScale})");
            var gauge = GameSceneInitializer.EvilSingleton.Gauge;
            gauge.StopMoving();
            gauge.SetPower(gravityScale, progress);
            _rigidbody.gravityScale = _initialGravityScale + gravityScale;
            _rigidbody.simulated = true;
            OnFalling?.Invoke();
            _hasFallen = true;
        }
    }
}