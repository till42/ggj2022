using System;
using UnityEngine;

namespace Seesaw.Logic
{
    public class ForcemeterBehaviour : MonoBehaviour
    {
        [SerializeField] private string _tag;

        [SerializeField] private ApplyForceBehaviour _applyForce;

        public Animator Animator;

        private void OnTriggerEnter2D(Collider2D otherCollider2D)
        {
            if (!otherCollider2D.CompareTag(_tag)) return;
            var otherRigidbody = otherCollider2D.GetComponentInParent<Rigidbody2D>();
            var velocity = otherRigidbody.velocity;
            var force = new Vector2(velocity.x, -velocity.y);
            _applyForce.Apply(force);
            Animator.SetTrigger("Pressed");
        }
    }
}