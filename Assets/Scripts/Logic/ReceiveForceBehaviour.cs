using Photon.Pun;
using UnityEngine;

namespace Seesaw.Logic
{
    public class ReceiveForceBehaviour : MonoBehaviour
    {
        private PhotonView _photonView;
        private Rigidbody2D _rigidbody2D;
        private Animator _animator;

        private void Awake()
        {
            _photonView = GetComponent<PhotonView>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _animator = GetComponentInChildren<Animator>();
        }

        [PunRPC]
        public void ReceiveForce(Vector2 force)
        {
            _animator.SetTrigger("Jump");
            if (PhotonNetwork.IsConnected && !_photonView.IsMine) return;
            _rigidbody2D.AddForce(force, ForceMode2D.Impulse);
        }
    }
}