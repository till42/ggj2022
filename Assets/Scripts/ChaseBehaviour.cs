using UnityEngine;

public class ChaseBehaviour : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _speed = 10f;


    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        var direction = (_target.transform.position - _rigidbody.position).normalized;
        _rigidbody.AddForce(direction * _speed);
    }
}