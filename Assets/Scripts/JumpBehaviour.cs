using UnityEngine;

public class JumpBehaviour : MonoBehaviour
{
    [SerializeField] private float _amplitude = 100f;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Jump()
    {
        var forceDirection = Vector3.up * _amplitude;
        _rigidbody.AddForce(forceDirection);
    }
}